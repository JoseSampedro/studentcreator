package com.privalia.presentation.unit.test;

import org.apache.log4j.Logger;
import org.junit.Test;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.util.Properties;

import com.privalia.dao.IDao;
import com.privalia.dao.INio;
import com.privalia.dao.StudentDao;
import com.privalia.model.Student;

import junit.framework.TestCase;

public class StudentDaoTest extends TestCase {
	private File studentsFile = new File("Students.txt");
	private static int idStudent = 0;
	
	static final Logger logger = Logger.getLogger(StudentDaoTest.class);
	static Properties prop = null;
	static FileInputStream input = null;
	
	protected void setUp() {
		prop = new Properties();
		try {
			input = new FileInputStream("src/main/resources/config.properties"); // podria userse GetResourcesStream
			prop.load(input);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new ExceptionInInitializerError(e);
		}
		if (studentsFile.exists()) {
			studentsFile.delete();
		}
	}
	
	@Test
	public void testAdd() throws IOException {
		Student studentSample = new Student(1, "Joe", "Pesci", 2654635);
		IDao<Student> studentDao = new StudentDao();
		studentDao.add(studentSample);
		
		BufferedReader br = new BufferedReader(new FileReader(studentsFile));
		String studentWrote = br.readLine();
		br.close();
		
		assertEquals(studentSample.toString(), studentWrote);
	} //MINDUNDI por no usar un findStudent
	
	@Test
	public void testAddWithNIO() throws IOException {
		Student studentSample = new Student(1, "Joe NIO", "Pesci", 2654635);
		INio<Student> studentDao = new StudentDao();
		studentDao.addWithNIo(studentSample);
		
		Student studentFound = findStudentWithNIo(studentSample.getIdStudent());
		assertEquals(studentFound, studentSample);
	}
	
	@Test
	public void testAddWithSQL() throws SQLException {
		Student student = new Student();
		IDao<Student> studentDao = new StudentDao();
		student.setName("Pepe");
		student.setSurname("Cohete");
		student.setAge(25);
		idStudent = studentDao.addWithSQL(student);
		assertTrue(idStudent > 0);
	}
	
	public Student findStudentWithNIo(int idAlumno) throws IOException {
		Student student = null;
		try (RandomAccessFile aFile = new RandomAccessFile(studentsFile, "rw");
			 FileChannel inChannel = aFile.getChannel()) {
			ByteBuffer buf = ByteBuffer.allocate(512);
			StringBuffer line = new StringBuffer();
			String[] studentString = null;
			boolean finished = false;
			
			while (inChannel.read(buf) > 0 && !finished) {
				buf.flip();
				for (int i = 0; i < buf.limit(); i++) {
					char ch = ((char) buf.get());
					if (ch == '\n') {
						String stringLine = line.toString();
						studentString = stringLine.split(",");
						if (Integer.parseInt(studentString[0]) == idAlumno) {
							student = Student.getStudent();
							student.setName(studentString[1]);
							student.setSurname(studentString[2]);
							student.setAge(Integer.parseInt(studentString[3].trim()));
							finished = true;
							break;
						}
						line = new StringBuffer();
					} else {
						line.append(ch);
					}
				}
				buf.clear();
		    }
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return student;
	}
}
