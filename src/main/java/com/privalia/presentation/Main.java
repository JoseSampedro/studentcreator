package com.privalia.presentation;

import com.privalia.dao.StudentDao;
import com.privalia.model.Student;
import com.privalia.util.MethodInfo;
import org.apache.log4j.Logger;
import java.util.Scanner;

public class Main {
	static final Logger logger = Logger.getLogger(Main.class);
	static final Scanner scanner = new Scanner(System.in);

	@MethodInfo(author = "Jose", revision = 2, comments = "First main!", date = "27/09/2017")
	public static void main(String[] args) {
		int optionSelected;

		do {
			System.out.println("Student-o-matic!. Elige una opción:");
			System.out.println("1. Agregar alumno");
			System.out.println("2. Salir");

			optionSelected = scanner.nextInt();

			switch (optionSelected) {
			case 1:
				System.out.println("Agregando alumno...");
				System.out.println("¿Qué nombre tiene el alumno?");
				String studentName = scanner.next();
				System.out.println("¿Qué apellido tiene el alumno?");
				String studentSurname = scanner.next();
				System.out.println("¿Qué edad tiene el alumno?");
				int studentAge = scanner.nextInt();
				System.out.println("¿Qué ID tiene el alumno?");
				int studentId = scanner.nextInt();

				Student studentSample = new Student(studentId, studentName, studentSurname, studentAge);
				new StudentDao().add(studentSample);
				logger.info("Student name: " + studentSample.getName());
				break;
			case 2:
				break;
			default:
				System.out.println("Opción incorrecta.");
				break;
			}
		} while (optionSelected != 2);
	}
}

