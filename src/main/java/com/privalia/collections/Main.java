package com.privalia.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Main {

	public static void main(String[] args) {
		Address address = new Address();
		List<Teacher> teacherList = new ArrayList<Teacher>();
		Teacher teacher1 = new Teacher();
		teacher1.setIdTeacher(1);
		teacher1.setName("Pepe");
		teacherList.add(teacher1);
		
		Teacher teacher2 = new Teacher();
		teacher2.setIdTeacher(2);
		teacher2.setName("Juan");
		teacherList.add(teacher2);
		
		Student student = new Student(address);
		student.setName("John");
		student.setSurname("Wayne");
		student.setAge(75);
		student.setIdStudent(2);
		student.setTeachers(teacherList);
		
		List studentsList = new ArrayList();
		int counter = 4;
		
		// Ejemplo de boxing
		studentsList.add(student);
		
		// Ejemplo de autoboxing
		studentsList.add(counter);
		
		Student newStudent = (Student) studentsList.get(0);
		String name = ((Student) studentsList.get(0)).getName();
		
		// Lista genérica (así no hace falta boxing y unboxing)
		List<Student> genericStudentList = new ArrayList<Student>();
		genericStudentList.add(student);
		Student newStudent2 = genericStudentList.get(0);
		
		Optional<Student> optionalStudent = genericStudentList.stream()
				.filter(p -> p.getName().equals("John")).findFirst();
		Student searchedStudent = optionalStudent.get();
		
		student.getTeachers().forEach(item->System.out.println(item));
		
		Product product2 = new Product();
		product2.setIdProduct(2);
		product2.setName("Another product");
		
		Product product1 = new Product();
		product1.setIdProduct(1);
		product1.setName("A product");
		
		Map <Long, Product> productList = new HashMap<Long, Product>();
		productList.put(1L, product1);
		productList.put(2L, product2);
		
		Category category1 = new Category();
		category1.setIdCategory(1);
		category1.setName("Cat1");
		category1.setProductList(productList);
	}
}
