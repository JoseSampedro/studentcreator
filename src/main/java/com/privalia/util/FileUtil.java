package com.privalia.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

public class FileUtil {
	
	static final Logger logger = Logger.getLogger(FileUtil.class);
	private static File file = null;
	
	private FileUtil() {
		// Does nothing as this is an utility class
	}
	
	@MethodInfo(author = "Jose", revision = 2, comments = "Create a file", date = "28/09/2017")
	public static synchronized boolean create(String path, String name) throws IOException  {
		boolean isFileCreated = false;
		StringBuilder filename = new StringBuilder();
		filename.append(path);
		filename.append(name);
		file = new File(filename.toString());
		
		if (file.exists()) {
			logger.warn("El fichero ya existe");
		} else {
			isFileCreated = file.createNewFile();
		}
		
		return isFileCreated;
	}
	
	@MethodInfo(author = "Jose", revision = 1, comments = "Returns the file", date = "4/10/2017")
	public static File getFile() {
		return file;
	}
	
	@MethodInfo(author = "Jose", revision = 1, comments = "Writes a line to a file", date = "28/09/2017")
	public static void writeLine(File file, IWritable item) throws IOException {
		StringBuilder writableItem = new StringBuilder();
		writableItem.append(item.toString());
		writableItem.append(System.getProperty("line.separator"));
		
		// Try-with-resources!
		try (FileWriter fw = new FileWriter(file, true)) {
			fw.write(writableItem.toString());	
		}
	}
}
