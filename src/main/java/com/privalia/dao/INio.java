package com.privalia.dao;

import java.io.IOException;

public interface INio<T> {
	int addWithNIo(T model) throws IOException;
}
