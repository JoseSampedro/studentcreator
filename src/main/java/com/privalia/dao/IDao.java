package com.privalia.dao;

import java.io.IOException;
import java.sql.SQLException;

public interface IDao<T> {
	int add(T model) throws IOException;
	int addWithSQL(T model) throws SQLException;
}
