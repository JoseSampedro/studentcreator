package com.privalia.dao;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import org.apache.log4j.Logger;

import com.mysql.jdbc.Statement;
import com.privalia.model.Student;
import com.privalia.util.FileUtil;

public class StudentDao extends BaseDao implements IDao<Student>, INio<Student> {

	static final Logger logger = Logger.getLogger(StudentDao.class);
	static Properties properties = null;
	static FileInputStream input = null;
	private Connection connect;

	static {
		properties = new Properties();
		try {
			input = new FileInputStream("src/main/resources/config.properties"); // podria userse GetResourcesStream
			properties.load(input);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new ExceptionInInitializerError(e);
		}
	}
	
	@Override
	public int add(Student student) {
		try {
			FileUtil.create(properties.getProperty("path"), properties.getProperty("filename"));
			FileUtil.writeLine(FileUtil.getFile(), student);
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		
		return student.getIdStudent();
	}

	@Override
	public int addWithNIo(Student student) throws IOException {
		Path path = Paths.get(properties.getProperty("filename"));
		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"), StandardOpenOption.APPEND)) {
			writer.write(student.toString());
			writer.write(System.lineSeparator());
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		}
		return student.getIdStudent();
	}
	
	@Override
	public int addWithSQL(Student student) throws SQLException{
		int idStudent = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			connect = super.getConnection();
			pstmt = connect.prepareStatement("INSERT INTO student(name, surname, age"+
					"VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1,student.getName());
			pstmt.setString(2,student.getSurname());
			pstmt.setInt(3, student.getAge());
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			rs.next();
			idStudent = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally{
			pstmt.close();
			rs.close();
			connect.close();
		}
		
		return idStudent;
	}
}
