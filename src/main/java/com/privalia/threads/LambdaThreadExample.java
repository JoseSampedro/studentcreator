package com.privalia.threads;

public class LambdaThreadExample {

	public static void main(String[] args) {
		// Método 1: heredando de Thread
		MyThread thread1 = new MyThread();
		
		// Método 2: creando el Thread pasándole la interfaz como parámetro
		Thread thread2 = new Thread(new MyRunnable());
		
		// Método 3: con función anónima para implementar la interfaz Runnable
		Runnable runnable2 = new Runnable() {
			@Override
			public void run() {
				System.out.println("Thread name : " + Thread.currentThread().getName());
			}
		};
		Thread thread3 = new Thread(runnable2);
		
		// Método 4: con lambda expression
		Runnable runnable3 = () -> {
			System.out.println("Thread name : " + Thread.currentThread().getName());
		};
		Thread thread4 = new Thread(runnable3);
		
		// "Ejecuto" los threads: no van en orden secuencial, el procesador irá ejecutando en paralelo
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
	}
}
